import io.vavr.collection.List;
import io.vavr.collection.Map;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ListTest{
    List<String> list = List.of("Java", "PHP", "Jquery", "Javascript", "JShell", "JAVA");

    @Test
    public void whenDropElement_thenCorrect(){
        List<String> list1 = list.drop(2);
        assertFalse(list1.contains("Java") && list1.contains("PHP"));

        List<String> list2 = list.dropRight(2);
        assertFalse(list2.contains("JAVA") && list2.contains("JShell"));

        List<String> list3 = list.dropUntil(s -> s.contains("Shell"));
        assertEquals(list3.size(), 2);

        List<String> list4 = list.dropWhile(s -> s.length() > 0);
        assertTrue(list4.isEmpty());
    }

    @Test
    public void whenTakeElement_thenCorrect(){
        List<String> list5 = list.take(1);
        assertEquals(list5.single(), "Java");

        List<String> list6 = list.takeRight(1);
        assertEquals(list6.single(), "JAVA");

        List<String> list7 = list.takeUntil(s -> s.length() > 6);
        assertEquals(list7.size(), 3);
    }

    @Test
    public void whenDistinctBy_thenCorrect(){
        List<String> list8 = list.distinctBy((s1, s2) -> s1.startsWith(s2.charAt(0) + "") ? 0 : 1);
        assertEquals(list8.size(), 2);
    }

    @Test
    public void whenIntersperse_thenCorrect(){
        String words = List.of("Boys", "Girls")
                .intersperse("and")
                .reduce((s1, s2) -> s1.concat(" " + s2))
                .trim();

        assertEquals("Boys and Girls", words);
    }

    @Test
    public void whenGroupedBy_thenCorrect(){
        Map<Boolean, List<String>> map = list.groupBy(s -> s.startsWith("J"));
        assertEquals(map.size(), 2);
        assertEquals(map.get(false).get().size(), 1);
        assertEquals(map.get(true).get().size(), 5);
    }

}
