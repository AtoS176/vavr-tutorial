import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Queue;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class QueueTest {
    @Test
    public void test(){
        Queue<Integer> queue = Queue.of(1, 2);
        Queue<Integer> secondQueue = queue.enqueueAll(List.of(4, 5));

        assertEquals(2, queue.size());
        assertEquals(4, secondQueue.size());

        Tuple2<Integer, Queue<Integer>> result = secondQueue.dequeue();
        assertEquals(Integer.valueOf(1), result._1);

        Queue<Integer> tailQueue = result._2;
        assertFalse(tailQueue.contains(secondQueue.get(0)));
    }
}
