import io.vavr.control.Option;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OptionTest {
    @Test
    public void givenValueWhenCreateOption(){
        Option<Object> noneOption = Option.of(null);
        Option<Object> someOption = Option.of("val");

        assertEquals("None", noneOption.toString());
        assertEquals("Some(val)", someOption.toString());
    }

    @Test
    public void givenNullWhenCreateOption(){
        String name = null;
        Option<String> nameOption = Option.of(name);

        assertEquals("beldung", nameOption.getOrElse("beldung"));
    }
}
