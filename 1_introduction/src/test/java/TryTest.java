import io.vavr.control.Try;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TryTest {
    @Test
    public void givenBadCode_WhenTryHandles_thenCorrect(){
        Try<Integer> computation = Try.of(() -> 1 / 0);

        assertTrue(computation.isFailure());
    }

    @Test
    public void givenBadCode_WhenTryHandle_thenCorrect2(){
        Try<Integer> computation = Try.of(() -> 1 / 0);
        int errorSentinel = computation.getOrElse(-1);

        assertEquals(-1, errorSentinel);
    }

    @Test
    public void givenBadCode_WhenTryHandle_thenCorrect3(){
        Try<Integer> computation = Try.of(() -> 1 / 0);

        assertThrows(ArithmeticException.class, () -> {
            computation.getOrElseThrow(() -> new ArithmeticException());
        });
    }
}
