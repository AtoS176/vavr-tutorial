import io.vavr.Tuple;
import io.vavr.Tuple2;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TupleTest {
    @Test
    public void whenCreateTuple2(){
        Tuple2<String, Integer> tuple = Tuple.of("Ala", 30);
        String element1 = tuple._1();
        Integer element2 = tuple._2();

        assertEquals("Ala", element1);
        assertEquals(30, element2);
    }
}
