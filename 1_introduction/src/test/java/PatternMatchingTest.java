import org.junit.jupiter.api.Test;

import static io.vavr.API.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PatternMatchingTest {
    @Test
    public void whenMatchWorks_thenCorrect(){
        int input = 2;
        String output = Match(input).of(
                Case($(1), "one"),
                Case($(2), "two"),
                Case($(3), "three"),
                Case($(), "?"));

        assertEquals("two", output);
    }
}
